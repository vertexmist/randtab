package org.randtab

import com.xenomachina.argparser.ArgParser
import com.xenomachina.argparser.InvalidArgumentException
import com.xenomachina.argparser.default
import org.bagofutils.unEscape

/**
 *
 */
class Config(parser: ArgParser) {

    val printGeneratorHelp by parser.flagging("-g", "--generatorHelp", help = "Print documentation for the generator file format.")
    val dir by parser.storing("-d", "--dir", help = "Directory to look for generators in (by default use current directory).  May also be a single generator file to use.").default(".")
    val input by parser.storing("-i", "--input", help = "Reads an input file, finds invocations for generators, replaces sections around them with generator output, and saves result to output.").default(null)
    val inputGeneratorMarker by parser.storing("--inputStartMarker", help = "String in the input file that will be followed by the generator id and optionally an input end marker.  Defaults to \"beginGenerating\" ").default("beginGenerating")
    val inputEndMarker by parser.storing("--inputEndMarker", help = "String in an input file that marks the end of the area that will be replaced by the previous generator invocation.  Defaults to \"endGenerating\"").default("endGenerating")
    val removeInputMarkers by parser.flagging("--removeInputMarkers", help = "If an input file is used, and this option specified, the start and end markers will be removed from the final output.")
    val output by parser.storing("-o", "--output", help = "The file to store the result to.  If not specified, the result is printed to the standard output.").default(null)
    val append by parser.flagging("-a", "--append", help = "If present, and the output is being saved to a file with -o, the output will be appended to the file instead of overwriting it.")
    val extension by parser.storing("-e", "--extension", help = "The filename extension used for generator modules.  Include any dot.  Defaults to '.generator'.").default(".generator")
    val number by parser.storing("-n", "--number", help = "Number of times the generator should be invoked.  By default one.") { toIntOrNull() ?: throw InvalidArgumentException("The value '$this' specified for the number parameter is not an integer.") }.default(1)
    val separator by parser.storing("--separator", help = "If the generator is invoked multiple times (n > 1), append this separator between each output of the generator.  By default a newline \\n.") { unEscape() }.default("\n")
    val generatorStartMarker by parser.storing("--start", help = "String in the generator files that indicates the start of generator source code.  All content before the start marker is ignored if it is specified.") { unEscape() }.default(null)
    val generatorEndMarker by parser.storing("--end", help = "String in the generator files that indicates the end of generator source code.  All content after the end marker is ignored if it is specified.") { unEscape() }.default(null)
    val prefixGeneratorName by parser.flagging("--prefixGeneratorName", help = "Append the name of the generator along with a newline before the output.")
    val prefix by parser.storing("--prefix", help = "Text to append before the output of the generator.  By default empty.") { unEscape() }.default("")
    val postfix by parser.storing("--postifx", help = "Text to append after the output of the generator.  By default empty.") { unEscape() }.default("")
    val source by parser.storing("-s", "--source", help = "Custom generator source provided directly as a parameter, to use in addition or instead of specifying a generator directory.  Optional.") { unEscape() }.default(null)
    val printVersion by parser.flagging("--version", help = "Print the current version of the program")
    val generator by parser.positionalList("GENERATOR_ID", "The identifier of the generator to invoke.  " +
                                                       "Use a dot-separated path with the directory and module in addition " +
                                                       "to the generator id.  " +
                                                       "(Example: mysubdir1.mysubdir2.modulefilename.generatorname ).  " +
                                                       "If the path is omitted, the first generator with a matching id is used.").default(emptyList())


}