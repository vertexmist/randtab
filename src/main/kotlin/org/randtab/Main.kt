package org.randtab

import com.xenomachina.argparser.*
import org.bagofutils.generator.GeneratorManager
import org.bagofutils.random.RandomSequence
import org.bagofutils.recursivelyMapFiles
import java.io.File


private val programName = "randtab"
private val version = "1.0.0"

val generatorDocs =
"""

== Generator file format definition ==

Generator File:
Contains zero or more imports:
import <dot-separated path to other module to import generators from>
After that contains zero or more generator definitions:
<output type> <generator id> = <generator expression>
Example:
import foodir.surnameModule
Text titleGenerator = "the " ~ pick(5: "good", 2: "great", "glorious", 0.5: "hypocritical", 0.2: "supercritical")
Text nameGenerator = pick("igor", "hugo", "orgus") ~ " " ~ titleGenerator ~ " " ~ surnameGenerator
Text manyPeople = repeat(2d10, nameGenerator ~ " and ") ~ nameGenerator

Type:
Type is one of Text, Num, Bool, Any, List< SomeOtherType >
If the type is followed by ? it may be null

Generator id:
Should start with a letter or underscore, and only contain ascii letters,
numbers, and underscores.  Identifiers starting with double underscores
are reserved for internal use.

Constants:
Text constants are surrounded by quotes '"':  "Hello World"
Number constants may be integers or real numbers: 1,  3.1415
Boolean constants are true or false.
nothing is null.

Expressions:
Supported arithmetic expressions for numbers are +, -, *, /
Supported comparison expressions are >, <, >=, <=
Supported equivalence expressions are == and !=
Supported boolean expressions are 'and' and 'or' and 'not'
Elvis expression: <expr1> ?: <expr2>
will return the result of expr2 if expr1 returns nothing (null)
Concatenation expression <expr1> ~ <expr2> will combine expr1 and expr2,
concatenating strings, adding numbers.  It's a shorthand for combine (see below).

Reference: <generator id>
A reference to another generator.  Invokes the other generator and returns its result.

Parentheses:
Can be used to group generator expressions.

Dice:
The dice expression can be used to create a random number: <count>d<sides>
will throw count number of dices with the specified number of sides and sum the result.
For example, 3d6 throws three six-sided dice and adds the results.

Combine: combine [result type] ( <expr1>, <expr2>, ... )
Combines the result of the expressions.  In case of text,
concatenates them, and in case of numbers, sums them.
The type of combine is the result type specified if present,
or otherwise calculated from the common type of the parameters.
~ is a shorthand for combine.
Example:
Text dwarfName = combine Text ("Olaf", "Oaken", "Shield")
Example:
Text dwarfName2 = "Igur" ~ "Long Beard"

Pick: pick [result type] ([weight1 :] <expr1>, [weight2 :] <expr2>, ...)
Selects one of the expressions randomly, weighted by the weights of the expressions.
The weight should be an expression of type Num.  If it is omitted, it will be 1.
Example:  Text orcName = pick(3: "Igor",
                              "Urgot",
                              (2+2): "Murgla")

List: list [element type] (<expr1>, <expr2>, ...)
Combines the specified expressions into a list.
Example: List<Text> names = list Text ("Fumir", "Barmir", "Zoomir")

Repeat: repeat [result type] ( <repeat count>, <expression> )
Repeats the specified expression the specified number of times, and combines the result.
Example: Num fourD6 = repeat (4, 1d6)
Example: Text orcHorde = repeat (1d10, orcName ~ ", ")

When: when [result type] (<condition1> : <result1>, <condition2> : <result2>, ... [, <resultIfAllFalse> )
Goes through the conditions and evaluates them, until a condition returns true,
then it evaluates and returns the result for that condition.  If no conditions
evaluate to true, and there is a last expression without a condition, it evaluates
and returns the last expression.  Otherwise if no conditions match and there is no
last expression, it returns nothing.
Example:
Text strength = when (hitPoints < 5: "weak",
                      hitPoints < 10: "average,
                      hitPoints < 15: "good",
                      "excellent")

"""


fun main(args: Array<String>) {
    mainBody(programName) {
        val helpFormatter = DefaultHelpFormatter("Parses generator files, and randomly generates output for the specified generator, either printing it to stdout, or to a file if -o is used.")
        val parser = ArgParser(args, helpFormatter = helpFormatter)
        val config = Config(parser)

        if (config.printGeneratorHelp) {
            println(generatorDocs)
            System.exit(0)
        }
        if (config.printVersion) {
            println("$programName-$version")
            System.exit(0)
        } else {
            try {
                generate(config)
            } catch (e: Throwable) {
                error("Error: ${e.message}")
            }
        }
    }
}

private fun generate(config: Config) {
    val manager = createGeneratorManager(config)

    val result = if (config.input != null) generateResultBasedOnInputFile(config, manager)
                 else generateResult(config, manager, config.number, getGeneratorName(config))

    outputResult(config, result)
}

private fun createGeneratorManager(config: Config): GeneratorManager {
    val rootPath = File(config.dir)
    if (!rootPath.exists()) error("Could not find the specified directory '$rootPath'")

    // Parse generators
    val fileProcessor: (File)->String = {
        var text = it.readText()
        val startMarker = config.generatorStartMarker
        val endMarker = config.generatorEndMarker
        if (startMarker != null) {
            text = text.substringAfter(startMarker, "")
            if (endMarker != null) text = text.substringBefore(endMarker)
        }
        text
    }

    val sources = { rootPath.recursivelyMapFiles({it.name.endsWith(config.extension)},
                                                 { true },
                                                 {it.map { it.name.removeSuffix(config.extension) }.joinToString(".")},
                                                 fileProcessor) }
    val manager = GeneratorManager(sources, emptyList(), config.source)
    return manager
}

private fun generateResult(config: Config,
                           manager: GeneratorManager,
                           count: Int,
                           generatorName: String): String {
    // Check inputs
    if (generatorName.isBlank()) {
        error("Missing generator id")
    }

    val result = StringBuilder()
    var first = true

    if (config.prefixGeneratorName) result.append(generatorName).append("\n")

    result.append(config.prefix)
    for (i in 1 .. count) {
        // Separator
        if (first) first = false else result.append(config.separator)

        // Generator
        result.append(manager.generate<Any>(generatorName).toString())
    }
    result.append(config.postfix)

    return result.toString()
}

fun generateResultBasedOnInputFile(config: Config, manager: GeneratorManager): String {
    val inputFile = File(config.input!!)
    if (!inputFile.exists()) error("Could not find the specified input file '$inputFile'")

    val startMarker = config.inputGeneratorMarker
    val endMarker = config.inputEndMarker
    if (startMarker.isBlank()) error("inputStartMarker should not be blank!")
    if (endMarker.isBlank()) error("inputEndMarker should not be blank!")

    var inputText = inputFile.readText()
    val result = StringBuilder()

    // Handle invocations
    while (inputText.contains(startMarker)) {

        // Skip until start marker
        val before = inputText.substringBefore(startMarker)
        result.append(before)
        if (!config.removeInputMarkers) result.append(startMarker)
        inputText = inputText.drop(before.length).drop(startMarker.length)

        // Read generator
        var controlLine = inputText.substringBefore("\n", "").substringBefore(endMarker)
        inputText = inputText.drop(controlLine.length)
        if (!config.removeInputMarkers) result.append(controlLine).append("\n")
        controlLine = controlLine.trim()
        val generator = controlLine.substringBefore(" ")
        val controlParams = controlLine.substringAfter(" ", "")

        // read parameters
        val count = readParam("num", controlParams) ?: readParam("count", controlParams) ?: 1
        val seed = readParam("seed", controlParams) ?: RandomSequence.default.nextLong()

        manager.random.setSeed(seed)

        // Generate
        try {
            result.append(generateResult(config, manager, count.toInt(), generator))
        }
        catch (e: Exception) {
            result.append("Error: " + e.message)
        }
        if (!config.removeInputMarkers) result.append("\n").append(endMarker)

        // Find end marker
        val endPos = inputText.indexOf(endMarker)
        val nextPos = inputText.indexOf(startMarker)
        if (endPos >= 0 && (nextPos < 0 || endPos < nextPos)) {
            // Remove content before end marker
            inputText = inputText.substringAfter(endMarker)
        }
    }

    // Append rest
    result.append(inputText)

    return result.toString()
}


private fun readParam(paramName: String, controlParams: String): Long? {
    var s = controlParams.substringAfter(paramName, "").trim()
    s = s.substringAfter(":", "").trim()
    s = s.substringBefore(" ").trim()
    return s.toLongOrNull()
}

private fun getGeneratorName(config: Config): String {
    // Only pick the first word, if a larger text area is selected
    return config.generator.joinToString(" ").trim().substringBefore(" ").substringBefore("\n").substringBefore("\t")
}

private fun outputResult(config: Config, result: String) {
    val output = config.output
    if (output != null) {
        val outFile = File(output)

        if (config.append) {
            outFile.appendText(result)
        } else {
            outFile.writeText(result)
        }
    } else {
        print(result)
    }
}

private fun error(msg: String) {
    System.err.println(msg)
    System.exit(1)
}
